<!--
TODO: Definir las secciones principales de la pagina
  <div class="">
    <body>
      <header> </header>
      <section class="schedule">
        <div class="subject-table"> </div>
        <div class="subject-list"> </div>
        </section>
      <div class="Querry-formulary"> </div>
      <div class = navigation > </div>
    </body>
  </div>

 -->

<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>proyecto Horarios</title>

    <link rel="stylesheet" href="css/sortable.css">
    <link rel="stylesheet" href="css/header.css">
    <link rel="stylesheet" href="css/nav.css">
    <link rel="stylesheet" href="css/footer.css">

    <script src= "https://code.jquery.com/jquery-3.1.1.min.js"></script>

    <script
			  src="http://code.jquery.com/ui/1.12.0/jquery-ui.js"
			  integrity="sha256-0YPKAwZP7Mp3ALMRVB2i8GXeEndvCq3eSl/WsAl1Ryk="
			  crossorigin="anonymous"></script>

   <script src= "js/sortable.js"></script>
   <script src= "js/listItems.js"></script>

  </head>


  <!-- Seccion de header -->
  <header>

     <div class="Welcome">
        <h1>Welcome to TheScheduler</h1>
        <a href="#">More than just a page web</a>
     </div>

  </header>

<!-- Cuerpo -->
  <body>

    <!-- Seccion de la pagina principal -->
    <section class="page">
      <!-- Seccion de el horario y la lista de materias -->
      <section class="schedule">
            <!-- Seccion para la tabla de materias -->
            <div class="subject-table">
              <table id ="TablaHorario" border="1">
                  <caption><strong>Horarios</strong></caption>
                  <thead>
                    <tr>
                      <th>Hora/dia</th>
                      <th>lunes</th>
                      <th>martes</th>
                      <th>miercoles</th>
                      <th>jueves</th>
                      <th>viernes</th>
                      <th>Sabado</th>

                    </tr>
                  </thead>

                  <tbody >
                    <!-- Filas para los dias de 7 a 8 -->
                    <tr>
                      <td id = Horario>
                        7am - 8am
                      </td>
                      <td>
                        <ul class="sortable">
                          <li>programacion1</li>
                          <li>programacion2</li>

                        </ul>
                      </td>
                      <td>
                        <ul class="sortable">
                          <li>programacion1</li>
                          <li>programacion2</li>

                        </ul>
                      </td>
                      <td>
                        <ul class="sortable">
                        

                        </ul>
                      </td>
                      <td>
                        <ul class="sortable">
                          <li>programacion1</li>
                          <li>programacion2</li>

                        </ul>
                      </td>
                      <td>
                        <ul class="sortable">
                          <li>programacion1</li>
                          <li>programacion2</li>
                        </ul>
                      </td>
                      <td>
                        <ul class="sortable">
                          <li>programacion1</li>
                          <li>programacion2</li>
                        </ul>
                      </td>

                    </tr>
                    <tr>
                      <!-- Filas para los dias de 8 a 9 -->
                      <td id = Horario>
                        8am - 9am
                      </td>
                      <td>
                        <ul class="sortable">
                          <li>programacion1</li>
                          <li>programacion2</li>

                        </ul>
                      </td>
                      <td>
                        <ul>
                          <ul class="sortable">


                        </ul>
                      </td>
                      <td>
                        <ul>
                          <ul class="sortable">
                            <li>programacion1</li>
                            <li>programacion2</li>

                        </ul>
                      </td>
                      <td>
                        <ul class="sortable">

                        </ul>
                      </td>
                      <td>
                        <ul class="sortable">
                          <li>programacion1</li>
                          <li>programacion2</li>

                        </ul>
                      </td>
                      <td>
                        <ul class="sortable">
                          <li>programacion1</li>
                          <li>programacion2</li>

                        </ul>
                      </td>



                        <!-- Filas para los dias de 9 a 10 -->
                    </tr>
                    <tr>
                      <td id = Horario>
                        9am - 10am
                      </td>
                      <td>
                        <ul class="sortable">
                          <li>programacion1</li>
                          <li>programacion2</li>

                        </ul>
                      </td>
                      <td>
                        <ul class="sortable">
                          <li>programacion1</li>
                          <li>programacion2</li>

                        </ul>
                      </td>
                      <td>
                        <ul class="sortable">
                          <li>programacion1</li>
                          <li>programacion2</li>

                        </ul>
                      </td>
                      <td>
                        <ul class="sortable">


                        </ul>
                      </td>
                      <td>
                        <ul class="sortable">


                        </ul>
                      </td>
                      <td>
                        <ul class="sortable">
                          <li>programacion1</li>
                          <li>programacion2</li>
                        </ul>
                      </td>

                    </tr>

                  <!-- Filas para los dias de 10 a 11 -->
                    <tr>
                      <td id = Horario>
                        10am - 11am
                      </td>
                      <td>
                        <ul class="sortable">
                          <li>programacion1</li>
                          <li>programacion2</li>

                        </ul>
                      </td>
                      <td>
                        <ul>
                          <ul class="sortable">
                            <li>programacion1</li>
                            <li>programacion2</li>

                        </ul>
                      </td>
                      <td>
                        <ul>
                          <ul class="sortable">


                        </ul>
                      </td>
                      <td>
                        <ul class="sortable">

                        </ul>
                      </td>
                      <td>
                        <ul class="sortable">
                          <li>programacion1</li>
                          <li>programacion2</li>

                        </ul>
                      </td>
                      <td>
                        <ul class="sortable">


                        </ul>
                      </td>




                    </tr>

                    <tr>
                      <td id = Horario>
                        11am - 12am
                      </td>
                      <td>
                        <ul class="sortable">
                          <li>programacion1</li>
                          <li>programacion2</li>

                      </td>
                      <td>
                        <ul class="sortable">
                          <li>programacion1</li>
                          <li>programacion2</li>

                      </td>
                      <td>
                        <ul class="sortable">
                          <li>programacion1</li>
                          <li>programacion2</li>

                      </td>
                      <td>
                        <ul class="sortable">


                      </td>
                      <td>
                        <ul class="sortable">
                          <li>programacion1</li>
                          <li>programacion2</li>

                      </td>
                      <td>
                        <ul class="sortable">
                          <li>programacion1</li>
                          <li>programacion2</li>

                      </td>


                    </tr>

                  <!-- Filas para los dias de 10 a 11 -->
                    <tr>
                      <td id = Horario>
                        10am - 11am
                      </td>
                      <td>
                        <ul class="sortable">


                        </ul>
                      </td>
                      <td>
                        <ul>
                          <ul class="sortable">
                            <li>programacion1</li>
                            <li>programacion2</li>

                        </ul>
                      </td>
                      <td>
                        <ul>
                          <ul class="sortable">
                            <li>programacion1</li>
                            <li>programacion2</li>

                        </ul>
                      </td>
                      <td>
                        <ul class="sortable">

                        </ul>
                      </td>
                      <td>
                        <ul class="sortable">


                        </ul>
                      </td>
                      <td>
                        <ul class="sortable">

                        </ul>
                      </td>



                        <!-- Filas para los dias de 12 a 1 -->
                    </tr>

                    <tr>
                      <td id = Horario>
                        12am - 1am
                      </td>
                      <td>
                        <ul class="sortable">
                          <li>programacion1</li>
                          <li>programacion2</li>

                      </td>
                      <td>
                        <ul class="sortable">
                          <li>programacion1</li>
                          <li>programacion2</li>

                      </td>
                      <td>
                        <ul class="sortable">


                      </td>
                      <td>
                        <ul class="sortable">
                          <li>programacion1</li>
                          <li>programacion2</li>

                      </td>
                      <td>
                        <ul class="sortable">


                      </td>
                      <td>
                        <ul class="sortable">
                          <li>programacion1</li>
                          <li>programacion2</li>

                      </td>


                        <!-- Filas para los dias de 1 a 2 -->
                    </tr>
                    <tr>
                      <td id = Horario>
                        1pm - 2pm
                      </td>
                      <td>
                        <ul class="sortable">
                          <li>programacion1</li>
                          <li>programacion2</li>

                      </td>
                      <td>
                        <ul class="sortable">
                          <li>programacion1</li>
                          <li>programacion2</li>

                      </td>
                      <td>
                        <ul class="sortable">


                      </td>
                      <td>
                        <ul class="sortable">

                      </td>
                      <td>
                        <ul class="sortable">
                          <li>programacion1</li>
                          <li>programacion2</li>

                      </td>
                      <td>
                        <ul class="sortable">
                          <li>programacion1</li>
                          <li>programacion2</li>

                      </td>

                      <!-- Filas para los dias de 2 a 3 -->
                    </tr>

                    <tr>
                    <td id = Horario>
                      2am - 3am
                    </td>
                    <td>
                      <ul class="sortable">
                        <li>programacion1</li>
                        <li>programacion2</li>

                    </td>
                    <td>
                      <ul class="sortable">


                    </td>
                    <td>
                      <ul class="sortable">


                    </td>
                    <td>
                      <ul class="sortable">


                    </td>
                    <td>
                      <ul class="sortable">
                        <li>programacion1</li>
                        <li>programacion2</li>

                    </td>
                    <td>
                      <ul class="sortable">


                    </td>

                    <!-- Filas para los dias de 3 a 4-->
                </tr>

                <tr>
                  <td id = Horario>
                    3am - 4am
                  </td>
                  <td>
                    <ul class="sortable">


                  </td>
                  <td>
                    <ul class="sortable">


                  </td>
                  <td>
                    <ul class="sortable">


                  </td>
                  <td>
                    <ul class="sortable">
                      <li>programacion1</li>
                      <li>programacion2</li>

                  </td>
                  <td>
                    <ul class="sortable">

                  </td>

                  <td>
                    <ul class="sortable">

                  </td>
                      <!-- Filas para los dias de 5 a 6 -->
                  </tr>

                  <tr>
                    <td id = Horario>
                      5am -6am
                    </td>
                    <td>
                      <ul class="sortable">
                        <li>programacion1</li>
                        <li>programacion2</li>

                    </td>
                    <td>
                      <ul class="sortable">
                        <li>programacion1</li>
                        <li>programacion2</li>

                    </td>
                    <td>
                      <ul class="sortable">
                        <li>programacion1</li>
                        <li>programacion2</li>

                    </td>
                    <td>
                      <ul class="sortable">
                        <li>programacion1</li>
                        <li>programacion2</li>

                    </td>
                    <td>
                      <ul class="sortable">
                        <li>programacion1</li>
                        <li>programacion2</li>

                    </td>

                    <td>
                      <ul class="sortable">
                        <li>programacion1</li>
                        <li>programacion2</li>

                    </td>

                  </tbody>

              </table>
            </div> <!-- subject-table -->


            <!-- Seccion para la barra de materias -->
            <div class="subject-list">
              <div class="subject-list-title">
                <h1>Lista de Materias</h1>
                <p>
                  Preparadurias:
                </p>
                <input type="text"  id="input_listName"/>

                <button type="submit" class="btn_sendMessage" id="btn_createList">
                  Create List
                </button>
              </div>
              <ul class="sortable" id="ul_current">
                <li>Programacion3</li>
                <li>Ayda</li>
                <li>Arc Computador</li>
                <li>Calculo</li>
              </ul>
            </div> <!-- subject-list -->

    </section> <!-- schedule -->

    <section class="Query-formulary">

      <!-- TODO: organize and set information then store it
                  Update information when page is updated-->
      <!-- send info to the server via get method -->
      <form action="demo_form_method.as" method="get" target="_blank">

          <p>
            <label> Querry:<br/>
              <input type="text" name="querry" size="40" ><br>
            </label>
          </p>
          <p>
            <input type="submit" value="Enviar">
            <input type="reset" value="Borrar">
          </p>

      </form>
      <p>Click on the submit button,
        and the input will be sent to
        a page on the server called
        "demo_form_method.asp".</p>
    </section>

  </body>

  <nav class="navigation">
    <ul>
      <li><a href="#">Profile</a></li>
      <li><a href="#">Settings</a></li>
      <li><a href="#">Notifications</a></li>
      <li><a href="#">Logout</a></li>
    </ul>
  </nav>

  <footer>
    <h3>Copyright (c) 2016 Copyright Holder All Rights Reserved.</h3>
  </footer>
</html>
