<?php
//FUNCIONES PARA EL CONTROLADOR FRONTAL

    function loadController($controller){

        //controlerNameControler in camel case
        $controlador=ucwords($controller).'Controller';
        $strFileController='controller/'.$controlador.'.php';

        if(!is_file($strFileController)){   //si no exite usar el controlador por defecto
            $strFileController='controller/'.ucwords(DEFAULT_CONTROLLER).'Controller.php';
        }

        require_once $strFileController;
        $controllerObj=new $controlador();  //crea un nuevo controlador
        return $controllerObj;
    }

    function loadAction($controllerObj,$action){
        $accion=$action;
        $controllerObj->$accion();  //ejecuta el metodo accion de controlador
    }

    function throwAction($controllerObj){
        //Si la accion esta definida
        if(isset($_GET["action"]) && method_exists($controllerObj, $_GET["action"])){
            loadAction($controllerObj, $_GET["action"]);
        }else{
            //Lanzar accion por defecto
            loadAction($controllerObj, DEFAULT_ACTION);
        }
    }

?>
