
<!-- De esta clase heredan todos los controladores -->

<?php

  class BaseController {

    public function __construct(){
      require_once 'BaseEntity.php';
      require_once 'BaseModel.php';

      //find all models in directory mode/
      foreach (glob("model/*.php") as $file) {
        require_once $file;
      }

    }

  /*
  * Este método lo que hace es recibir los datos del controlador en forma de array
  * los recorre y crea una variable dinámica con el indice asociativo y le da el
  * valor que contiene dicha posición del array, luego carga los helpers para las
  * vistas y carga la vista que le llega como parámetro. En resumen un método para
  * renderizar vistas.
  */
    public function view ($vista, $datos){
      foreach($datos as $id_assoc => $valor)
      {
        ${$id_assoc} = $valor;
      }

      require_once 'core/ViewHelpers.php';
      $helper = new ViewHelper();
      //OJO aca     //directorio/nombreVista+View.php
      require_once 'view/'.$vista.'View.php';
    }



    /*
    LA redireccion permite mantener la logica de la navegacion y del posicionamiento
    de un sitio web, incluso si los archivos han cambiado de lugar
    */
    public function redirect($controlador=DEFAULT_CONTROLLER,
                             $accion=DEFAULT_ACTION){

       header("Location:index.php?controller=".$controlador."&action=".$accion);
   }

  }




 ?>
