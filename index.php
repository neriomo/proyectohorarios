<?php
//Configuración global
require_once 'config/global.php';

//Base para los controladores
require_once 'core/BaseController.php';

//Funciones para el controlador frontal
require_once 'core/FrontalController.php';


//Cargamos controladores y acciones
if(isset($_GET["controller"])){
    $controllerObj=loadController($_GET["controller"]);
    throwAction($controllerObj);

}else{
    //Si no esta definido ingrese al controlador por defecto
    $controllerObj=loadController(DEFAULT_CONTROLLER);
    throwAction($controllerObj);

}
?>
