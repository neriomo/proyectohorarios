$(function functionName() {
    var remove =false;
    $('ul.sortable').sortable({
      connectWith:'ul.sortable',
      dropOnEmpty:true,
      distance: 0.8,
      tolerance:"pointer",
      forcePlaceHolderSize: true,
      over: function(){
        remove= false;
      },
      out: function () {
        remove= true;
      },
      beforeStop: function (event,ui) {
        if(remove == true)
        {
          ui.item.remove();
        }
      }
    });

    $('ul.sortable').sortable({
      connectWith:'ul.sortable'
    });



});
