﻿/*Proyecto Horarios*/




--Los dias de la semana
CREATE DOMAIN DIA_HORARIO VARCHAR(3)
CONSTRAINT set_of_days
	CHECK (VALUE IN ('Lun','Mar','Mie','Jue','Vie','Sab'));



--Debe ser NN-MM donde NN < MM y NN y MM entre 07-18
CREATE DOMAIN HORA_FACTIBLE VARCHAR(5)		--tested
CONSTRAINT valid_schedule
	CHECK (VALUE ~'(0[7-9]-0[8-9])|(0[7-9]-1[0-8])|(10-1[1-8])|(11-1[2-8])|(12-1[3-8])|(13-1[4-8])|(14-1[5-8])|(15-1[6-8])|(16-1[7-8])|(17-18)');

--Los tipos de semestres
CREATE DOMAIN SEMESTRE VARCHAR(20)
CONSTRAINT set_of_semesters
	CHECK (VALUE IN ('1','2','3','4','5','6','7','8','9','Electiva'));


--Las secciones son 0X donde X se estima estar entre 1 y 39
CREATE DOMAIN SECCION VARCHAR(8)
CONSTRAINT valid_section
	CHECK (VALUE ~'0[1-9]|1[0-9]|2[0-9]|3[0-9]');




CREATE TABLE CURSO(
nombre 		varchar(60) NOT NULL,
seccion		SECCION NOT NULL,
semestre	SEMESTRE NOT NULL,
Horas_semana	integer DEFAULT 0,	--Si es la induccion al servicio comunitario entonces asumo tiene cero horas
CHECK(Horas_semana >= 0),
CONSTRAINT PK_CURSO PRIMARY KEY (nombre,seccion));--No existen dos cursos con el mismo nombre y seccion


--Un curso puede tener varios horarios
CREATE TABLE HORARIO (
nombreCurso	varchar(60),
seccionCurso	SECCION,
dia		DIA_HORARIO NOT NULL,
cantHorasDia	HORA_FACTIBLE NOT NULL,
salon		varchar(20) NOT NULL,

CONSTRAINT PK_HORARIO PRIMARY KEY (nombreCurso,seccionCurso,dia,cantHorasDia,salon),

CONSTRAINT FK_HORARIO 
FOREIGN KEY (nombreCurso,seccionCurso) REFERENCES CURSO (nombre,seccion) on delete cascade on update cascade
);
