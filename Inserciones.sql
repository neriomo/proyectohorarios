﻿/** Inserciones bases de datos Horarios **/
/** Horios del semestre U-2016**/

select nombre,seccion,semestre,horas_semana,dia,canthorasdia,salon from curso join horario on (curso.nombre = horario.nombrecurso and curso.seccion = horario.seccioncurso);
select * from horario;
select * from curso;

--	Ingenieria de Sistemas
insert into curso values('Ingenieria de Sistemas','01',1,4);
insert into horario values('Ingenieria de Sistemas','01','Mar','07-09','2O05');
insert into horario values('Ingenieria de Sistemas','01','Jue','07-09','2O05');

--	Programacion 1
insert into curso values('Programacion 1','01',1,6);
insert into horario values('Programacion 1','01','Lun','13-15','2S09');
insert into horario values('Programacion 1','01','Mar','13-15','2S09');
insert into horario values('Programacion 1','01','Jue','13-15','2S08');

insert into curso values('Programacion 1','02',1,6);
insert into horario values('Programacion 1','02','Lun','09-11','2S09');
insert into horario values('Programacion 1','02','Mar','09-11','2S09');
insert into horario values('Programacion 1','02','Mie','09-11','2S09');

insert into curso values('Programacion 1','03',1,6);
insert into horario values('Programacion 1','03','Lun','07-09','2S09');
insert into horario values('Programacion 1','03','Mar','07-09','2S09');
insert into horario values('Programacion 1','03','Jue','07-09','2S08');

insert into curso values('Programacion 1','04',1,6);
insert into horario values('Programacion 1','04','Lun','13-15','2S09');
insert into horario values('Programacion 1','04','Mar','13-15','2S09');
insert into horario values('Programacion 1','04','Jue','13-15','2S09');

--Programacion 2

insert into curso values('Programacion 2','01',2,6);
insert into horario values('Programacion 2','01','Mie','13-15','2S09');
insert into horario values('Programacion 2','01','Jue','13-15','2S09');
insert into horario values('Programacion 2','01','Vie','13-15','2S09');

insert into curso values('Programacion 2','02',2,6);
insert into horario values('Programacion 2','02','Lun','09-11','2O07');
insert into horario values('Programacion 2','02','Jue','09-11','2S09');
insert into horario values('Programacion 2','02','Vie','09-11','2O07');


--Arquictectura Computadoras

insert into curso values('Arquictectura Computadoras','01',3,6);
insert into horario values('Arquictectura Computadoras','01','Mar','07-09','4O08');
insert into horario values('Arquictectura Computadoras','01','Mie','07-09','2S09');
insert into horario values('Arquictectura Computadoras','01','Jue','07-09','4O08');

insert into curso values('Arquictectura Computadoras','02',3,6);
insert into horario values('Arquictectura Computadoras','02','Lun','15-17','4O08');
insert into horario values('Arquictectura Computadoras','02','Mie','15-17','4O08');
insert into horario values('Arquictectura Computadoras','02','Jue','15-17','2S09');


--Programacion 3

insert into curso values('Programacion 3','01',2,6);
insert into horario values('Programacion 3','01','Lun','09-11','2O14');
insert into horario values('Programacion 3','01','Mie','09-11','2O14');
insert into horario values('Programacion 3','01','Vie','07-09','2S09');


insert into curso values('Programacion 3','02',2,6);
insert into horario values('Programacion 3','02','Mar','09-11','4O08');
insert into horario values('Programacion 3','02','Jue','09-11','4O08');
insert into horario values('Programacion 3','02','Vie','09-11','2S09');


